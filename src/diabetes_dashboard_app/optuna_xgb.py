from config import bin_path,experiment_name

STUDY_NAME = experiment_name
OPTUNA_STORAGE_DB_URI = f"sqlite://{bin_path.joinpath(f'{experiment_name}_optuna.db').as_posix()}"
FITTED_XGBR_FNAME = bin_path.joinpath(f"{experiment_name}_xgbr.json").as_posix()

# from functools import cached_property

N_FOLDS = 10
XGB_OBJECTIVE = "reg:squarederror"
XGB_EVAL_METRIC = "rmse"
NUM_BOOST_ROUNDS = 1000  # 10000
EARLY_STOPPING_ROUNDS = 30  # 100

N_OPTUNA_TRIALS = 10  # 1000


DIRECTION = "minimize"  #'maximize

xgb_params = {
    "objective": XGB_OBJECTIVE,
    "eval_metric": XGB_EVAL_METRIC,
    "sampling_method": "gradient_based",
    "tree_method": "gpu_hist", # add method to check for GPU availibility
}


class TunedXGBR:
    def __init__(self):
        self._X_y = {}
        self._model = None

    @property
    def X_y(self):
        if self._X_y == {}:
            from .data import fetch_diabetes
            X, y = fetch_diabetes(return_X_y=True)
            self._X_y = {"X": X, "y": y}
        return self._X_y

    @property  # todo: refactor using cached_property: https://docs.python.org/3/library/functools.html#functools.cached_property
    def X(self):
        return self.X_y["X"]

    @property
    def y(self):
        return self.X_y["y"]

    def tune_model(self):
        import xgboost as xgb
        import optuna

        xgb_full = xgb.DMatrix(self.X, label=self.y)

        def objective(trial):

            param = {
                **xgb_params,
                **{
                    "booster": trial.suggest_categorical(
                        "booster", ["gbtree"]
                    ),  # ,"dart"]),
                    "lambda": trial.suggest_loguniform("lambda", 1e-7, 1.0),
                    "alpha": trial.suggest_loguniform("alpha", 1e-7, 1.0),
                    "subsample": trial.suggest_float("subsample", 0, 1.0),
                },
            }

            if param["booster"] in ["gbtree", "dart"]:
                param["max_depth"] = trial.suggest_int("max_depth", 1, 9)
                param["min_child_weight"] = trial.suggest_int("min_child_weight", 1, 20)
                param["eta"] = trial.suggest_loguniform("eta", 1e-7, 1.0)
                param["gamma"] = trial.suggest_loguniform("gamma", 1e-7, 1.0)
                param["grow_policy"] = trial.suggest_categorical(
                    "grow_policy", ["depthwise", "lossguide"]
                )
            if param["booster"] == "dart":
                param["sample_type"] = trial.suggest_categorical(
                    "sample_type", ["uniform", "weighted"]
                )
                param["normalize_type"] = trial.suggest_categorical(
                    "normalize_type", ["tree", "forest"]
                )
                param["rate_drop"] = trial.suggest_loguniform("rate_drop", 1e-7, 1.0)
                param["skip_drop"] = trial.suggest_loguniform("skip_drop", 1e-7, 1.0)

            # Add a callback for pruning.
            pruning_callback = optuna.integration.XGBoostPruningCallback(
                trial, f"test-{XGB_EVAL_METRIC}"
            )

            xgb_cv_results = xgb.cv(
                param,
                xgb_full,
                num_boost_round=NUM_BOOST_ROUNDS,
                early_stopping_rounds=EARLY_STOPPING_ROUNDS,
                nfold=N_FOLDS,
                callbacks=[pruning_callback],
            )

            trial.set_user_attr("n_estimators", len(xgb_cv_results))

            return xgb_cv_results[f"test-{XGB_EVAL_METRIC}-mean"].values[-1]

        self.study = optuna.create_study(
            study_name=STUDY_NAME,
            direction=DIRECTION,
            storage=OPTUNA_STORAGE_DB_URI,
            load_if_exists=True, #todo: check whether this gives an error if the db does not exist
        )

        self.study.optimize(objective, n_trials=N_OPTUNA_TRIALS)

    @property
    def optuna_params(self):
        import optuna
        
        try:
            self.study = optuna.load_study(
                study_name=STUDY_NAME, storage=OPTUNA_STORAGE_DB_URI
            )
        except:
            self.tune_model()
        return {
            **self.study.best_params,
            **xgb_params,
            **self.study.best_trial.user_attrs,
        }

    def fit_model(self):
        from xgboost import XGBRegressor
        self._model = XGBRegressor(**self.optuna_params)

        self._model.fit(self.X, self.y)
        self._model.save_model(FITTED_XGBR_FNAME)

    @property
    def model(self):
        from xgboost import XGBRegressor
        try:
            self._model = XGBRegressor(**self.optuna_params)
            self._model.load_model(FITTED_XGBR_FNAME)
        except:
            self.fit_model()
        return self._model

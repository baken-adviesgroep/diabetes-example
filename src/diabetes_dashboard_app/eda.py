import os
from config import static_path,experiment_name

PROFILE_STORAGE_PATH = static_path.joinpath(f"{experiment_name}_EDA_profile.html")


class DiabetesEDA:
    def __init__(self):
        self._data = None

    @property
    def data(self):
        if not self._data:
            from . import fetch_diabetes

            self._data = fetch_diabetes()
        return self._data

    @property
    def description(self):
        return self.data.DESCR

    @property
    def profile(self):
        if not PROFILE_STORAGE_PATH.is_file():
            self.create_and_store_profile()
        
        with open(PROFILE_STORAGE_PATH.as_posix(), "r", encoding='utf-8') as f:
            return f.read() 

    def create_and_store_profile(self):
        
        from pandas_profiling import ProfileReport

        ProfileReport(
            df=self.data, 
            title="Diabetes Report", 
            dark_mode=True
        ).to_file(PROFILE_STORAGE_PATH.as_posix())

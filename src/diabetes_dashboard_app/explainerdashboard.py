from explainerdashboard import ExplainerDashboard
from dash_bootstrap_components.themes import FLATLY
from config import bin_path,experiment_name

DASHBOARD_FNAME = bin_path.joinpath(f"{experiment_name}_dashboard.yaml")
EXPLAINER_FNAME = bin_path.joinpath(f"{experiment_name}_explainer.joblib")


class DiabetesExplained:
    def __init__(self):
        self._explainer_dashboard = None

    def create_explainerdashboard(self):
        from .data import fetch_diabetes
        from .optuna_xgb import TunedXGBR
        from explainerdashboard.explainers import XGBRegressionExplainer

        X, y = fetch_diabetes(return_X_y=True)

        explainer = XGBRegressionExplainer(TunedXGBR().model, X, y)
        self._explainer_dashboard = ExplainerDashboard(
            explainer,
            summary_type="detailed",
            hide_type=True,
            bootstrap=FLATLY,
            title = 'Diabetes Prediction Explained',
            cv = 10,
            hide_poweredby = True
        )

    def store_explainerdashboard(self):

        self._explainer_dashboard.to_yaml(
            DASHBOARD_FNAME,
            dump_explainer=True,
            explainerfile = EXPLAINER_FNAME)

    def load_explainerdashboard(self):
        self._explainer_dashboard = ExplainerDashboard.from_config(EXPLAINER_FNAME,DASHBOARD_FNAME)

    @property
    def dashboard(self):
        if DASHBOARD_FNAME.is_file() and EXPLAINER_FNAME.is_file():
            self.load_explainerdashboard()

        else:
            self.create_explainerdashboard()
            self.store_explainerdashboard()

        return self._explainer_dashboard

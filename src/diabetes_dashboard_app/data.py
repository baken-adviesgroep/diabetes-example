# todo: replace this data with the kaggle dataset


def fetch_diabetes(return_X_y:bool=False):
    import pandas as pd
    from sklearn.datasets import load_diabetes
    data = load_diabetes()

    X = pd.DataFrame(data=data["data"], columns=data["feature_names"])
    y = data["target"]
    X["sex"] = X["sex"].apply(lambda x: x > 0) * 1
    X.rename(columns={"sex": "male"}, inplace=True)
    data = pd.concat([X, pd.Series(y, name="target")], axis=1)
    if return_X_y:
        return X, y

    return data


def fetch_diabetes_prediction(return_X_y:bool=False):
    from src.config import data_path
    import pandas as pd
    data = pd.read_csv(data_path.joinpath('diabetes.csv').as_posix())
    
    if return_X_y:
        target_col = 'class'
        X,y = data.drop[target_col],data[target_col]
        return X,y

    return data

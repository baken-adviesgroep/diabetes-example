from pathlib import Path
import os

base_dir = Path(os.path.abspath(os.path.dirname(__file__)))
static_path = base_dir.joinpath("static")
data_path = base_dir.joinpath("data")
bin_path = base_dir.joinpath("artifacts")

experiment_name = 'diabetes'

has_gpu = False 

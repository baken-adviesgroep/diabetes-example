from diabetes_dashboard_app import DiabetesExplained
import os
from config import static_path
from flask import Flask, render_template


# app = Flask(__name__)

# app.config.requests_pathname_prefix = ''
db = DiabetesExplained().dashboard
app = db.flask_server()
# db.server = app
# db.url_base_pathname = "/explain_predictions"


# @app.route("/explain_predictions")
# def return_dashboard():
#     return db.app.index()

# @app.route('/')
# def return_index():
#     return render_template('index.html')


# @app.route("/explore_data")
# def return_eda():
#     from diabetes_dashboard_app.eda import DiabetesEDA

#     return DiabetesEDA().profile


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))

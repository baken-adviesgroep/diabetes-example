pip install poetry --quiet
poetry export --dev --without-hashes --format requirements.txt --output requirements.txt
pip install -r requirements.txt --quiet